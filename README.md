# Ansible playbooks

This repository contains a basic monitoring system based on Telegraf / InflxuxDB / Grafana.
It also allows to execute some maintenance tasks on the given servers.

## Installation

To install role dependencies, you must use the following command :

```bash
ansible-galaxy install -r roles/requirements.yml
```

## Usage

This playbook is divided in 3 parts : bootstrap, monitoring, agents.

The first one allow to globally configure the servers, then the last two are
specific configurations.

Run them using :

```bash
$ ./playbooks/00_bootstrap.yml
$ ./playbooks/01_monitoring.yml
$ ./playbooks/02_agents.yml
```

Or use the global playbook file :

```bash
$ ./playbooks/cluster.yml
```
